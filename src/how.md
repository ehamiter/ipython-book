# How to read this book

This book was generated using a Rust program called [mdBook](https://github.com/rust-lang/mdBook) that has several goodies baked in. It handles the layout and responsive design so my focus can lie on the content of this book instead of the architecture of the site. I also use footnotes from time to time. <sup class="footnote-reference"><a href="#fn1" id="ref1">1</a></sup>

As far as the features go, you can:

…toggle the chapter menu by clicking on the hamburger menu: <i class="fa fa-fw fa-bars"></i>

…change the theme by selecting the paintbrush icon: <i class="fa fa-fw fa-paint-brush"></i>

…search the book using the magnifying glass: <i class="fa fa-fw fa-search"></i>

…turn pages by clicking the left and right angles:<i class="fa fa-fw fa-angle-left"></i> <i class="fa fa-fw fa-angle-right"></i>

You can also navigate by using your keyboard's left and right arrows.

A warning symbol will appear by a section that has potentially tricky parts that require close attention:

> <i class="fa fa-fw fa-warning"></i> Don't run on wet tiled surfaces.

For code snippets, you can copy the input by clicking on the copy symbol:
```
ipython --version
```
The resulting output is shown in a color-inverted block:
<pre class="output">
7.18.1
</pre>

---

<sup class="footnote-definition" id="fn1">1. Like this one.<a href="#ref1" title="Jump back to footnote 1 in the text.">↩</a></sup>
