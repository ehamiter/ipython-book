# This book is not free

Well… it's free as in freedom, but not free as in beer. Although you don't necessarily have to spend money to buy it.

*Huh?*

You are most welcome (and encouraged) to [buy me a coffee / beer / pizza](https://buymeacoffee.com/ehamiter) to show your appreciation if you enjoy this book and you get something out of it— you learn some new tricks, speed up your efficiency, save your company tens of millions of dollars, and so forth.

If you're strapped for cash, waiting for your Bitcoins to mature, or what have you— you can make this book better. I'm a big believer in open source software, and this book is no different. Spot a <strike>tpyo</strike> typo? See something grievously wrong? Want to clarify an example? Make a pull request, and make a difference for the next person who reads this book. Click the GitHub icon <i class="fa fa-github"></i> on the top right of the page to visit the repo.

If you don't want to part with your hard-earned money, and have no desire to alter the content via pull requests, then tell someone about it. Mention this book to a friend, colleague, or enemy. Post a link to a forum you frequently visit. Tweet a link to it. The more people that see it, the better— for everyone. More eyes on the content ensures mistakes are caught, wrongs are righted, and knowledge is shared. Python programming efficiency soars, the global economy recovers, and cats and dogs live together in harmony.