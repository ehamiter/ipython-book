# Introduction

Per the first blurb on [IPython's web page](https://ipython.org/), it is described as a rich architecture for interactive computing with:

> A kernel for [Jupyter](https://jupyter.org/).  
> Support for interactive data visualization and use of [GUI toolkits](https://ipython.org/ipython-doc/stable/interactive/reference.html#gui-event-loop-support).  
> Flexible, [embeddable interpreters](https://ipython.org/ipython-doc/stable/interactive/reference.html#embedding-ipython) to load into your own projects.  
> Easy to use, high performance tools for [parallel computing](https://ipyparallel.readthedocs.io/en/latest/).  

You can see that IPython has a veritable treasure trove of very powerful features. This book won't be covering *any* of those.

Instead, we are going to explore the interactive shell via the command line that one would typically use in conjunction with Python web development, typically when working with a Django / Flask / Tornado / other Python web framework.

As far as specific versions go, this book will cover Python 3.8.2 and IPython 7.18.1.
